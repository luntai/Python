#Filter
def is_odd(n):
    return n % 2 == 1
L = [1,2,3,4,5,6]
print(list(filter(is_odd, L)))

def not_empty(s):
    return s and s.strip()
print(list(filter(not_empty, ['A', '', 'B', None, 'C', '  '])))

#埃氏晒素数
def _odd_iter():
    n = 1
    while True:
        n = n + 2
        yield n

def _not_divisible(n):
    return lambda x: x % n != 0

def primes(): #primes()也是一个无限序列
    yield 2
    it = _odd_iter() #初始化奇数序列
    while True:
        n = next(it) #返回素数序列的第一个
        yield n
        it = filter(_not_divisible(n), it) #过滤掉所有的素数n 的倍数

for n in primes():
    if n < 5:
        print(n)
    else:
        break

#练习：过滤掉所有回数 12321 909

def is_palindrome(n):
    s = str(n)
    range_ = int(len(s) / 2)
    len_ = int(len(s))
    for i in range(0, range_):
        if s[i] != s[len_ - i - 1]:
            return n
# 测试:
output = filter(is_palindrome, range(1, 1000))
print(list(output))

#Sort排序
a_list = [36, 5, -12, 9, -21]
print(sorted(a_list))
print(sorted(a_list, key = abs))
print(sorted(a_list, reverse = True))
print(sorted(['bob', 'about', 'Zoo', 'Credit'], key=str.lower))

#练习：
'''假设我们用一组tuple表示学生名字和成绩：
L = [('Bob', 75), ('Adam', 92), ('Bart', 66), ('Lisa', 88)]
请用sorted()对上述列表分别按名字排序：'''
def by_name(t):
    return t[0]
L = [('Bob', 75), ('Adam', 92), ('Bart', 66), ('Lisa', 88)]
L2 = sorted(L, key=by_name)
print(L2)
L3 = sorted(L, key = lambda t: t[0])
print(L3)

#返回函数
def calc_sum(*args):#*args 表示多个参数但是并没有给他们起名字
    ax = 0
    for n in args:
        ax = ax + n
    return ax
print(calc_sum(1,2,3))

def  lazy_sum(*args):
    def sum():
        ax = 0
        for n in args:
            ax = ax + n
        return ax
    return sum
f = lazy_sum(1,2,3)
print(f())
'''在这个例子中，我们在函数lazy_sum中又定义了函数sum，并且，
内部函数sum可以引用外部函数lazy_sum的参数和局部变量，
当lazy_sum返回函数sum时，相关参数和变量都保存在返回的函数中，
这种称为“闭包（Closure）”的程序结构拥有极大的威力。

请再注意一点，当我们调用lazy_sum()时，每次调用都会返回一个新的函数，即使传入相同的参数：
'''
f1 = lazy_sum(1, 3, 5, 7, 9)
f2 = lazy_sum(1, 3, 5, 7, 9)
print(f1== f2)
'''False'''

#闭包
def count():
    fs = []
    for i in range(1, 4):
        def f():
            return i * i
        fs.append(f)
    return fs
f = list(count())
print(f[0](), f[1](), f[2]())

def count2():
    def f(j):
        def g():
            return j * j
        return g
    fs =[]
    for i in range(1, 4):
        fs.append(f(i))
    return fs
f1, f2, f3 = count2()
print(f1(), f2(), f3())

#匿名函数  参数x  返回值表达式 x*x
print(list(map(lambda x: x * x, [1,2,3,4,5,6,7,8,9])))
def build(x, y):
    return lambda: x * x + y * y
print(build(1,2)())
decoration = build
print(decoration(1,2)())
print(decoration.__name__)

#Decorator装饰器
def log(func):
    def wrapper(*args, **kw):#将原函数包装了一下 打印log之后 返回去了
        print('call %s():' %func.__name__)
        return func(*args, **kw)
    return wrapper
@log
def now():
    print('2016-6-15 16:36')
now()
print(now.__name__)
'''把@log放到now()函数的定义处，相当于执行了语句：now = log(now)'''

def log(text):
    def decorator(func):
        def wrapper(*args, **kw):
            print('%s %s():' % (text, func.__name__))
            return func(*args, **kw)
        return wrapper
    return decorator
@log('execute')
def now():
    print('2015-3-25')
now()
print(now.__name__)
'''把@log放到now()函数的定义处，相当于执行了语句：now = log('execute')(now)
首先执行log('execute') return decorator函数
然后调用返回的函数，参数是now函数， return wrapper函数。
经过decorator包装过的函数now的名字 已经变成了wrapper了'''

import functools

def log(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        print('call %s():' % func.__name__)
        return func(*args, **kw)
    return wrapper
@log
def now():
    print('2015-3-25')
print(now.__name__)

#偏函数
def int2(x, base=2):
    return int(x, base)
print(int2('1000000'))
print(int('1000000',2))

import  functools
int2 = functools.partial(int, base = 2)
print(int2('1001001'))
print(int2('1001001',base = 10))
