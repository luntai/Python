class Student(object):
    def __init__(self, name, score):
        self.name=name
        self.score = score
    def print_score(self):
        print('%s: %f' % (self.name, self.score))

bart = Student('Bart Simpson', 59)
lisa = Student('Lisa Simpson', 87)
bart.print_score()
lisa.print_score()
print(bart)
print(lisa)
bart.score = 1243

class Student2(object):
    def __init__(self, name, score):
        self.__name = name
        self.__score = score
    def print_socre(self):
        print("%s : %s" %(self.__name, self.__score))
bart2 = Student2("bart2", 23.3)
print(bart2._Student2__name)

class Animal(object):
    def run(self):
        print("Animal is runing ...")

class Dog(Animal):
    def run(self):
        print("Dog is runing.,..")

class Cat(Animal):
    def run(self):
        print("Cat is runing.,..")

dog = Dog()
dog.run()
cat = Cat()
print(isinstance(dog, Dog))
print(isinstance(dog, Animal))

#多态
def run_twice(animal):
    animal.run()
    animal.run()
run_twice(dog)
run_twice(cat)
'''多态的好处就是，当我们需要传入Dog、Cat、Tortoise……时，我们只需要接收Animal类型就可以了，
因为Dog、Cat、Tortoise……都是Animal类型，然后，按照Animal类型进行操作即可。
由于Animal类型有run()方法，因此，传入的任意类型，只要是Animal类或者子类，
就会自动调用实际类型的run()方法，这就是多态的意思：
'''

print(len("ABC"))
print('ABC'.__len__())

#属性
class MyObject(object):
    def __init__(self):
        self.x = 9
    def power(self):
        return self.x * self.x
obj = MyObject()
print(obj)
print(hasattr(obj, 'x'), hasattr(obj, 'y'))
setattr(obj, 'y', 19)
print(obj.y)

#动态绑定属性 和 方法
class Student(object):
    pass
s = Student()
s.name = 'Michael'
print(s.name)

def set_age(self, age):
    self.age = age

from types import MethodType
s.set_age = MethodType(set_age, s)
s.set_age(25)
print(s.age)
'''这种方法只能给一个实例绑定方法'''
'''为了给所有势力都绑定，那么可以给class绑定'''

def set_score(self, score):
    self.score = score

Student.set_score = set_score
s.set_score(100)
print(s.score)
s2 = Student()
s2.set_score(99)
print(s2.score)

class Student(object):
    __slots__ = ('name', 'age')
s = Student()
s.name = 'Michael' #绑定属性
s.age = '12'
#s.score = 32 绑定分数出错

#@property
class Student(object):
    def __init__(self):
        self._score = 0

    def set_score(self, value):
        if not isinstance(value, int):
            raise ValueError('Score must be an integer!')
        if value < 0 or value  > 100:
            raise ValueError('Score must between 0~100!')
        self._score = value

    def get_score(self):
        if self._score:
            return self._score
        else:
            return None

s = Student()
print(s.set_score(100))
print('asdfasdfasdfadsf',s.get_score())

class Student(object):
    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, value):
        if not isinstance(value, int):
            raise ValueError('score must be an integer!')
        if value < 0 or value > 100:
            raise ValueError('score must between 0 ~ 100!')
        self._score = value

s = Student()
s.score = 61
print(s.score)

#练习：利用@property给一个Screen对象加上width和height属性，以及一个只读属性resolution：
class Screen(object):
    @property
    def width(self):
        return self._width
    @width.setter
    def width(self, value):
        if not isinstance(value, float):
            raise ValueError('width must be an float')
        if value < 0:
            raise ValueError('width must bigger than 0')
        self._width = value

    @property
    def height(self):
        return self._height
    @height.setter
    def height(self, value):
        if not isinstance(value, float):
            raise ValueError('width must be an float')
        if value < 0:
            raise ValueError('width must bigger than 0')
        self._height = value

    #不定义setter方法就是一个只读属性：
    @property
    def resolution(self):
        return self._height * self._width
s = Screen()
s.width = 1024.0
s.height = 768.0
print(s.resolution)

#多重继承
'''比如：动物分类，eg: Dog, Cat, Bat, Ostrich, Parrot'''
class Animal(object):
    pass
#哺乳动物 鸟类
class Mammal(Animal):
    pass
class Bird(Animal):
    pass

# 各种动物:
class Dog(Mammal):
    pass

class Bat(Mammal):
    pass

class Parrot(Bird):
    pass

class Ostrich(Bird):
    pass

#给动物加上 Runnanle 和 Flyable功能
class Runnable(object):
    def run(self):
        print('Running...')

class Flyable(object):
    def fly(self):
        print('Flying...')

#通过多重继承，一个子类就可以同时获得多个父类的所有功能。 C++也可以
class Dog(Mammal,Runnable):
    pass
def Bat(Mammal, Flyable):
    pass
Dog().run()

#MixIn的目的就是给一个类增加多个功能，这样，在设计类的时候，
#我们优先考虑通过多重继承来组合多个MixIn的功能，而不是设计多层次的复杂的继承关系。
class RunnableMixIn(object):
    def run(self):
        print('Running...')

class CarnivorousMixIn(object):
    def eat(self):
        print('Eating meat...')

class Tiger(Mammal, RunnableMixIn, CarnivorousMixIn):
    pass
Tiger().eat()