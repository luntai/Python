#定制类
class Student(object):
    def __init__(self, name):
        self._name = name
    def __str__(self):
        return 'Student object (name: %s)' %self._name
    __repr__ = __str__ #repr是测试时候 程序开发者看到的字符串

s = Student('LeeJimson')
print(s)

class Fib(object):
    def __init__(self):
        self.a, self.b = 0 , 1 #初始化两个值
    def __iter__(self):
        return self
    def __next__(self):
        self.a, self.b = self.b, self.a + self.b
        if self.a > 1000:
            raise StopIteration();
        return self.a
    def __getitem__(self, n):
        if isinstance(n, int):#索引
            a, b = 1, 1
            for x in range(n):
                a, b = b, a + b
                return a
        elif isinstance(n, slice):
            start = n.start
            stop = n.stop
            if start is None:
                start = 0
            a, b = 1, 1
            L = []
            for x in range(stop):
                if x >= start:
                    L.append(a)
                a, b = b,  a + b
            return L
for n in Fib():
    print(n)
print(Fib()[2])
print(Fib()[1:10])

class Chain(object):
    def __init__(self, path = ''):
        self._path = path

    def __getattr__(self, path):
        return Chain('%s/%s' %(self._path, path))

    def __str__(self):
        return self._path

    __repr__ = __str__

print(Chain().status.user.timeline.list)

class Student(object):
    def __init__(self, name):
        self.name = name

    def __call__(self):#直接对实例进行调用
        print('My name is %s.' %self.name)

s = Student('Michael')
s()
#通过判断一个变量是否能被调用，来判断一个变量是对象还是函数
#callable判断一个对象是否被调用
print(callable(Student('Lee')))
print(callable(max))
