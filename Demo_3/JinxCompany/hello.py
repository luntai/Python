#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'a test module'
__author__ = 'Jimson'

import sys


def test():
    args = sys.argv
    if len(args) == 1:
        print('Hello world!')
    elif len(args) == 2:
        print('Hello, %s!' % args[1])
    else:
        print('Too many arguments!')


# 只有通过命令函直接运行hello模块时候,name = main这样才会执行测试模块
if __name__ == '__main__':
    test()